dimension==0.8.2   
matplotlib==3.4.2
numpy==1.21.0
pandas==1.2.5
plotly==5.1.0
plotly_express==0.4.1
streamlit==0.83.0